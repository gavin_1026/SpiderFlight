package com.cuckoo.spider.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author lovepier
 * @version 创建时间：2016年11月3日 下午5:52:37 程序的简单说明
 */
public class InitListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent paramServletContextEvent) {

        Properties prop = new Properties();
        InputStream in = InitListener.class.getResourceAsStream("/config.properties");
        try {
            prop.load(in);
            String DCity1 = prop.getProperty("DCity1").trim();//出发城市
            String ACity1 = prop.getProperty("ACity1").trim();//到达城市
            String time = prop.getProperty("time").trim();
            String url = prop.getProperty("url").trim();
            Timer timer = new Timer();
            Task task = new Task(url,DCity1,ACity1);
            // 安排指定的任务在指定的时间开始进行重复的固定延迟执行。
            timer.schedule(task, 0, (long) Integer.parseInt(time));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void contextDestroyed(ServletContextEvent paramServletContextEvent) {

    }
}
