package com.cuckoo.spider.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.FileUtils;

/**
 * @author lovepier
 * @version 创建时间：2016年11月3日 下午3:18:59 定时器
 */
public class Task extends TimerTask {
    private  String url;
    private  String DCity1;
    private  String ACity1;

    public Task(String url, String DCity1, String ACity1) {
        this.url = url;
        this.DCity1 = DCity1;
        this.ACity1 = ACity1;
    }

    public void run() {
        String[] DCitys = DCity1.split(",");
        String[] ACitys = ACity1.split(",");
        List<String> dates = param();
        for (String DCity : DCitys) {
            for (String ACity : ACitys) {
                for (String date : dates) {
                    try {
                        SpiderUtils.downPageFromUrl(url, DCity, ACity, date);
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                   
                }
            }
        }

    }

    /**
     * 参数
     * 
     * @return
     */
    public static List<String> param() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dates = new ArrayList<String>();
        dates.add(sdf.format(new Date()));
        for (int i = 1; i <= 90; i++) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date date = calendar.getTime();
            dates.add(sdf.format(date));
        }
        return dates;
    }

}
