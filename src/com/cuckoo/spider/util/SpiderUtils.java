package com.cuckoo.spider.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang3.StringUtils;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Spider Utils
 * 
 * @author authurzhang
 * 
 */
public class SpiderUtils {

    private static List<WebClient> webClients = new ArrayList<WebClient>();
    static {
        WebClient webClient1 = new WebClient(BrowserVersion.INTERNET_EXPLORER_11);
        WebClient webClient2 = new WebClient(BrowserVersion.INTERNET_EXPLORER_8);
        WebClient webClient3 = new WebClient(BrowserVersion.INTERNET_EXPLORER_9);
        WebClient webClient4 = new WebClient(BrowserVersion.CHROME);
        webClients.add(webClient1);
        webClients.add(webClient2);
        webClients.add(webClient3);
        webClients.add(webClient4);
    }

    /**
     * 下载Page信息
     * 
     * @param url
     * @param css
     * @param js
     * @return
     */
    public static void downPageFromUrl(String url, String DCity1, String ACity1, String DDate1) {
        // System.out.println(url);
        WebClient webClient = null;
        try {
            int index = (int) Math.round(Math.random() * (webClients.size() - 1));
            // webClient.getOptions().setCssEnabled(css);
            // webClient.getOptions().setJavaScriptEnabled(js);
            // webClient.setJavaScriptTimeout(0 * 1000);
            // return webClient.getPage(url);
            // WebClient webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER_8);
            webClient = webClients.get(index);
//            webClient.getOptions().setProxyConfig(new ProxyConfig("58.218.213.116", 10074));
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setRedirectEnabled(true);

            webClient.getOptions().setThrowExceptionOnScriptError(false);

            webClient.getOptions().setTimeout(50000);
            String path = url + "&DCity1=" + DCity1 + "&ACity1=" + ACity1 + "&DDate1=" + DDate1;
            System.out.println(path);
            HtmlPage htmlPage = webClient.getPage(path);
//            System.out.println(htmlPage.asText());
            webClient.waitForBackgroundJavaScript(30000);
            webClient.closeAllWindows();
            parseXml(htmlPage, DCity1, ACity1, DDate1);
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println("访问受限");
        }

    }

    /**
     * 解析
     * 
     * @param doc
     */
    public static void parseXml(HtmlPage htmlPage, String DCity1, String ACity1, String DDate1) {
        List<List<String>> listRow = new ArrayList<List<String>>();
        List<String> mapHead = new ArrayList<String>();
        String pageXml = htmlPage.asText();
        if (!pageXml.isEmpty()) {

            JSONObject js = JSON.parseObject(pageXml);
            JSONObject als = (JSONObject) js.get("als");// 航空的名字
            JSONArray fis = (JSONArray) js.get("fis");// 所有航空的航班
            mapHead.add(0, "航空公司");
            mapHead.add(1, "机型");
            mapHead.add(2, "出发城市-到达城市");
            mapHead.add(3, "出发机场-到达机场");
            mapHead.add(4, "出发时间-到达时间");
            mapHead.add(5, "准点率");
            mapHead.add(6, "代理");
            mapHead.add(7, "退改票");
            mapHead.add(8, "优惠");
            mapHead.add(9, "折扣");
            mapHead.add(10, "价格");
            mapHead.add(11, "剩余票数");
            listRow.add(0, mapHead);
            listRow.add(1, new ArrayList<String>());
            for (int i = 0; i < fis.size(); i++) {
                JSONObject result = (JSONObject) fis.get(i);
                String dcc = (String) result.get("dcc");// 出发城市
                String acc = (String) result.get("acc");// 到达城市
                String dpc = (String) result.get("dpc");// 出发机场编码
                String apc = (String) result.get("apc");// 到达机场编码
                String dpbn = (String) result.get("dpbn");// 出发机场名字
                String apbn = (String) result.get("apbn");// 到达机场名字
                String dt = (String) result.get("dt");// 起飞时间
                String at = (String) result.get("at");// 到达时间
                String fn = (String) result.get("fn");// 航班信息

                JSONObject cf = (JSONObject) result.get("cf");// 飞机型号信息
                String type = (String) cf.get("c");// 飞机型号
                String s = (String) cf.get("s");// 飞机大小
                String size = "";// 飞机大小
                if (s.equalsIgnoreCase("L")) {
                    size = "大型";
                } else if (s.equalsIgnoreCase("M")) {
                    size = "中型";
                } else {
                    size = "小型";
                }
                String confort = (String) result.get("confort");// 其他信息
                JSONObject confortJson = JSON.parseObject(confort);
                String punctuality = (String) confortJson.get("HistoryPunctuality");// 准点率
                JSONArray scs = (JSONArray) result.get("scs");// 详细信息

                for (int j = 0; j < scs.size(); j++) {

                    List<String> listCol = new ArrayList<String>();
                    JSONObject result2 = (JSONObject) scs.get(j);//
                    Integer price = (Integer) result2.get("p");// 价格

                    Integer mq = (Integer) result2.get("mq");// 剩余机票
                    String mq2 = "";
                    if (mq == 0) {
                        mq2 = "";
                    } else {
                        mq2 = String.valueOf(mq);
                    }
                    String rt = (String) result2.get("rt");// 折扣
                    JSONObject tgq = (JSONObject) result2.get("tgq");// 退改信息
                    String c = (String) result2.get("c");// 座次档次
                    String level = "";
                    if (c.equalsIgnoreCase("F")) {
                        level = "头等";
                    } else if (c.equalsIgnoreCase("C")) {
                        level = "商务";
                        rt = "公务" + rt;
                    } else if (c.equalsIgnoreCase("Y")) {
                        level = "经济";
                    }
                    String sn = (String) result2.get("sn");// 超值
                    String dd = "";
                    if (StringUtils.isNotBlank(sn)) {
                        dd = sn + level;
                        String desc = (String) result2.get("desc");// 描述
                    }

                    // System.out.println(level+price + "--" + rt + "--" + result2.get("rate"));
                    String edn = (String) tgq.get("edn");// 签转条件
                    String ned = (String) tgq.get("ned");// 是否允许改签。F:可以签转，T:不得签转,H：允许签转（需要满足条件）
                    // System.out.println(edn + "--" + ned);
                    Integer mef = (Integer) tgq.get("mef");// 同舱改期金额
                    Integer mrf = (Integer) tgq.get("mrf");// 退票费
                    String rfn = (String) tgq.get("rfn");// 退票费
                    String al = (String) tgq.get("al");// 航空公司代码
                    String airlineCompanyName = (String) als.get(al);// 航空公司名字
                    String agent = "";// 代理
                    JSONArray ics = (JSONArray) result2.get("ics");// 共享信息

                    if (ics.size() == 0) {
                        agent = "优选快速出票";
                    } else {
                        for (Object object : ics) {
                            JSONObject result3 = (JSONObject) object;//
                            String IconType = (String) result3.get("IconType");
                            if (IconType.equalsIgnoreCase("AirLineMarketing")) {
                                agent = airlineCompanyName + "直销";
                            } else if (IconType.equalsIgnoreCase("BestBusiness")) {
                                agent = "优选快速出票";
                            } else if (IconType.equalsIgnoreCase("Limit") || IconType.equalsIgnoreCase("PackagePriority")) {
                                agent = "特惠";
                            }
                        }
                    }
                    /*
                     * if(fn.equalsIgnoreCase("KN5988")){ System.out.println(agent+"====="+price); System.out.println(ics); }
                     */
                    String mefStr = "";// 同舱改期描述
                    if (mrf > 0) {
                        if (mef > 0) {
                            mefStr = "退改￥" + mef + "起";
                        } else if (mef == 0) {
                            mefStr = "免费改期";
                        } else {
                            mefStr = rfn;
                        }
                    } else if (mrf == 0) {
                        if (mef > 0) {
                            mefStr = "免费退票";
                        } else if (mef == 0) {
                            mefStr = "免费退改";
                        } else {
                            mefStr = rfn;
                        }
                    } else {
                        mefStr = rfn;
                    }
                    if (j == 0) {
                        listCol.add(0, airlineCompanyName + fn);
                        listCol.add(1, type + "(" + size + ")");
                        listCol.add(2, dcc + "-" + acc);
                        listCol.add(3, dpbn + "-" + apbn);
                        listCol.add(4, dt + "-" + at);
                        listCol.add(5, punctuality);
                        listCol.add(6, agent);
                        listCol.add(7, mefStr);
                        listCol.add(8, dd);
                        listCol.add(9, rt);
                        listCol.add(10, String.valueOf(price));
                        listCol.add(11, mq2);
                    } else {
                        listCol.add(0, "");
                        listCol.add(1, "");
                        listCol.add(2, "");
                        listCol.add(3, "");
                        listCol.add(4, "");
                        listCol.add(5, "");
                        listCol.add(6, agent);
                        listCol.add(7, mefStr);
                        listCol.add(8, dd);
                        listCol.add(9, rt);
                        listCol.add(10, String.valueOf(price));
                        listCol.add(11, mq2);
                    }

                    /*
                     * for (Object object3 : details) { JSONObject result3 = (JSONObject) object3;// String price = (String) result3.get("p"); System.out.println(price); }
                     */
                    if (!listRow.contains(listCol)) {

                        listRow.add(listCol);
                    }
                }
                listRow.add(new ArrayList<String>());
                // System.out.println(dt + "---" + dcc + "---" + acc + "---" + apc + "---" + dpc + "---" + apbn + "---" + dpbn + "---" + at + "---" + fn + "---" + punctuality);
            }

            if (!listRow.isEmpty()) {
//                System.out.println(listRow);
                writeExcel(listRow, DCity1, ACity1, DDate1);
            }

        }
    }

    public static String dateFormat(String format_pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_pattern);
        Date date = new Date();
        String time = sdf.format(date);
        return time;

    }

    /**
     * 写入到excel
     * 
     * @param path
     */
    public static void writeExcel(List<List<String>> lists, String DCity1, String ACity1, String DDate1) {

        String path = "/data/spider/flight/" + DDate1.replaceAll("-", "") + "/" + DCity1 + "-" + ACity1 + "/" + dateFormat("yyyyMMddHHmmss") + ".xls";
        if (createFile(path)) {
            WritableWorkbook wb;
            try {
                File file = new File(path);
                wb = Workbook.createWorkbook(file);
                WritableSheet ws = wb.createSheet("sheet1", 0);
                for (int i = 0; i < lists.size(); i++) {
                    List<String> listc = lists.get(i);
                    for (int j = 0; j < listc.size(); j++) {
                        Label label = new Label(j, i, listc.get(j));
                        ws.addCell(label);
                    }
                }
                wb.write();
                wb.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (RowsExceededException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (WriteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static boolean createFile(String path) {

        boolean bl = false;
        File file = new File(path);
        if (!file.getParentFile().exists()) {
            // 如果目标文件所在的目录不存在，则创建父目录
            if (file.getParentFile().mkdirs()) {
                bl = true;
            }
        } else {
            bl = true;
        }
        try {

            if (bl) {
                if (file.createNewFile()) {
                    System.out.println("创建单个文件" + path + "成功！");
                    return true;
                }
            }

        } catch (IOException e) {
            System.out.println("创建单个文件" + path + "失败！" + e.getMessage());
        }
        return false;

    }

    public static void main(String[] args) throws IOException {
        // dateFormat("yyyyMMddHHmmss");

        // System.out.println(param());
        // helloHtmlUnit();
        // task to run goes here
        // readURL("D:/parser/url.txt");// 存放url的文件，每行一个
        // HtmlPage htmlPage = SpiderUtils.downPageFromUrl("http://flights.ctrip.com/domesticsearch/search/SearchFirstRouteFlights?SearchType=S", "SHA", "BJS", "2016-12-20");
        /*
         * Pattern p = Pattern.compile("\\s*|\t|\r|\n");
         * 
         * Matcher m = p.matcher(htmlPage.asText());
         * 
         * String dest = m.replaceAll(""); System.out.println(dest);
         */
        /*
         * Elements tables = doc.select(".flight-list-content"); System.out.println(tables.size());
         */
        /*
         * for (Element table : tables) {
         * 
         * Elements td = tr.get(0).getElementsByTag("td"); Elements table2 = td.get(0).getElementsByTag("table"); Element td2 = table2.get(0).getElementById("mainContent");
         * 
         * FileUtils.write(new File("D:/sdfs.txt"), table.toString()); System.out.println(table); }
         */
        // Map<Integer, Map<Integer, String>> map = new HashMap<Integer, Map<Integer, String>>();
        // Elements tables = doc.select("#J_RoomListTbl");
        // System.out.println(tables.size());

        // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间

        /*
         * Timer timer = new Timer(); Task task = new Task("D:/parser/url.txt"); // 安排指定的任务在指定的时间开始进行重复的固定延迟执行。 timer.schedule(task, 0, 10*60*1000);
         */

    }

}
